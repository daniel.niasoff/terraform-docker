FROM alpine:3.12 as build

ENV TERRAFORM_VERSION 0.14.6

RUN apk add --no-cache wget unzip openssh
RUN mkdir -p /var/lib/jenkins/.ssh \
    && ssh-keyscan gitlab.com >> /var/lib/jenkins/.ssh/known_hosts 
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && mv terraform /usr/local/bin

FROM alpine:3.12

ENV JENKINS_UID 113
ENV JENKINS_GID 119

COPY --from=build /var/lib/jenkins/.ssh/known_hosts /var/lib/jenkins/.ssh/known_hosts
COPY --from=build /usr/local/bin/terraform /usr/local/bin

RUN addgroup jenkins -g ${JENKINS_GID} \
    && adduser jenkins -u ${JENKINS_UID} -G jenkins -h /var/lib/jenkins -D \
    && chown -R ${JENKINS_UID}:${JENKINS_GID} /var/lib/jenkins \
    && apk add --no-cache py3-lxml py3-pip git openssh-client \
    && pip3 install packaging==20.4 \
    && pip3 install terraform-compliance


USER jenkins:jenkins
